import UIKit

class FetchController: NSObject {
    
    //MARK: --
    //MARK: Initializers
    
    let apiLink: String = "https://api.github.com/users"
    
    //This class is layer betwen Model structures and Fetch abstract class, so he knows about all of them,
    //but no one of them knows about this class. Only ViewControllers knows about this FetchController for taking
    //information and representing it at screen
    
    var users: Users = Users()
    var subscribers: Users = Users()
    
    static let shared: FetchController = FetchController()
    private override init() {}
    
    //MARK: --
    //MARK: Public mathods
    
    //Dispatch asynchronious in background for preventing screen freezes while download and making stuff on completion for
    //give possibility to get to main thread for making UI representing stuff.
    
    func fetchUsers(from id: Int, onCompletion: @escaping () -> Swift.Void) {
        Fetch.fetchLast(with: self.apiLink, userId: id, completion: { json in
            for item in json {
                let dict: [String: Any] = item as! [String: Any]
                let user: User = User.init(with: dict)
                self.users.addModel(user: user)
            }
            DispatchQueue.main.async {
                onCompletion()
            }
        })
    }
    
    func fetchSubscribers(from user: Int, onCompletion: @escaping () -> Swift.Void) {
        let login: String = "\(apiLink)/\(self.users.model(at: user).login!)/followers?per_page=10"
        
        DispatchQueue.global(qos: .background).async {
            Fetch.fetchSubscribers(from: login, completion: {json in
                var newSubscribers: Users = Users()
                
                for item in json {
                    let dict: [String: Any] = item as! [String: Any]
                    let user: User = User.init(with: dict)
                    newSubscribers.addModel(user: user)
                }
                self.subscribers = newSubscribers
                
                DispatchQueue.main.async {
                    onCompletion()
                }
            })
        }
    }
}
