import UIKit

class UsersTableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    //Making method for call outside of Cell class
    
    func makeCell(withUser user: User) {
        self.avatar.image = user.avatar?.image
        self.loginLabel.text = user.login
        self.linkLabel.text = user.profileLink
    }
    
}
