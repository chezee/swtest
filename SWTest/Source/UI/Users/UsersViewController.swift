import UIKit

class UsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: --
    //MARK:Initializers
    
    @IBOutlet weak var tableView: UITableView!
    
    let usersCellIdentifier: String = "UsersCellIdentifier"
    
    //MARK: --
    //MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UsersTableViewCell.classForCoder(),
                                forCellReuseIdentifier: usersCellIdentifier)
        self.tableView.register(UINib.init(nibName: "UsersTableViewCell", bundle: nil),
                                forCellReuseIdentifier: usersCellIdentifier)

        //Fetching data when main controller is loading. It could be called at start
        //in AppDelegate or making some prepare setting for start, but don't have sense for this task
        
        FetchController.shared.fetchUsers(from: 0, onCompletion: {
                    self.tableView.reloadData()
        })
    }
    
    //MARK: --
    //MARK: Tableview datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FetchController.shared.users.count()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UsersTableViewCell = tableView.dequeueReusableCell(withIdentifier: usersCellIdentifier) as! UsersTableViewCell
        cell.makeCell(withUser: FetchController.shared.users.model(at: indexPath.row))
        cell.selectionStyle = .none

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let count: Int = FetchController.shared.users.count()
        
        //If visibleCell has indexPath equal to (total count of users - 10) we start to download new page of users
        //In expectation was: "application should allow to browse at least first 100 users"
        //So if we have 100 users in our table method stops to add users
        
        if count < 100 && indexPath.row == count - 10{
            let lastID: Int = FetchController.shared.users.lastUser().id
            FetchController.shared.fetchUsers(from: lastID, onCompletion: {
                DispatchQueue.main.sync {
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    //MARK: --
    //MARK: Tableview delegate
    
    //Preparing to push Subscribers controller via starting download subscribers from internet
    //When downloading is finished contreller is initiated from nib and pushed by NavigationController
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        FetchController.shared.fetchSubscribers(from: indexPath.row, onCompletion: {
            let subscribers: SubscribersViewController = SubscribersViewController(nibName: "SubscribersViewController", bundle: nil)
            subscribers.title = FetchController.shared.users.model(at: indexPath.row).login
            self.navigationController?.pushViewController(subscribers, animated: true)
        })
    }
}
