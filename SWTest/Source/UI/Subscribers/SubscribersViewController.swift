import UIKit

class SubscribersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: --
    //MARK: Initializers
    
    @IBOutlet weak var tableView: UITableView!
    
    let subscribersCellIdenfitifer = "subscribersCellIdentifier"
    
    //MARK: --
    //MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(SubscribersTableViewCell.self, forCellReuseIdentifier: subscribersCellIdenfitifer)
        self.tableView.register(UINib.init(nibName: "SubscribersTableViewCell", bundle: nil), forCellReuseIdentifier: subscribersCellIdenfitifer)
    }
    
    //MARK: --
    //MARK: Tableview datasource/delegate methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FetchController.shared.subscribers.count()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SubscribersTableViewCell = tableView.dequeueReusableCell(withIdentifier: subscribersCellIdenfitifer) as! SubscribersTableViewCell
        cell.user = FetchController.shared.subscribers.model(at: indexPath.row)
        return cell
    }
    
    //MARK: --
    //MARK: Swipe for cell
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //Because I can :)
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let swipe = UITableViewRowAction(style: .normal, title: "Remove", handler: {action, index in
            self.removeCell(at: index.row)
        })
        swipe.backgroundColor = .blue
        
        return [swipe]
    }
    
    func removeCell(at index: Int) {
        FetchController.shared.subscribers.removeModelAtIndex(index: index)
        self.tableView.reloadData()
    }
    
}
