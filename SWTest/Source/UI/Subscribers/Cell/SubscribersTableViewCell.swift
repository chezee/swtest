import UIKit

class SubscribersTableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    //Using setter for making Cell UI
    
    var user: User = User(){
        willSet(newUser) {
            self.avatar.image = newUser.avatar?.image
            self.linkLabel.text = newUser.profileLink
            self.nameLabel.text = newUser.login
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
}
