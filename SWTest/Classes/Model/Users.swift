import UIKit

//Users structure is used like main Model in this application. It has private only one Array variable.
//That was made for preventing other behavior than was predicted by programmer

struct Users {
    
    //MARK: --
    //MARK: Initializers
    
    private var arrayOfUsers: Array<User>
    
    init() {
        self.arrayOfUsers = Array<User>()
    }
    
    //MARK: --
    //MARK: Common methods
    
    func model(at index: Int) -> User {
        return self.arrayOfUsers[index]
    }
    
    func count() -> Int {
        return self.arrayOfUsers.count
    }
    
    func lastUser() -> User {
        return self.arrayOfUsers.last!
    }
    
    //MARK: --
    //MARK: Mutating methods
    
    mutating func addModel(user: User) {
        self.arrayOfUsers.append(user)
    }
    
    mutating func deleteModel(user: User) {
        let i: Int = self.arrayOfUsers.index(of: user)!
        self.arrayOfUsers.remove(at: i)
    }
    
    mutating func removeModelAtIndex(index: Int) {
        self.arrayOfUsers.remove(at: index)
    }
    
    mutating func addModels(array: Array<User>) {
        for item in array {
            self.addModel(user: item)
        }
    }
}
