import UIKit

//Structure for downloading image from interner.

struct ImageModel {
    var image: UIImage = UIImage()
    
    init(withURL: String) {
        let url: URL = URL(string: withURL)!
        do {
            let data: Data = try Data(contentsOf: url)
            self.image = UIImage(data: data)!
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
