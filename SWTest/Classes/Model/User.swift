import UIKit

//User model is made for collect data and fast represent when it needs. Also made with protocol Equatable for
//possibility to compare by ID which is has to be unique

struct User: Equatable {
    
    let id: Int
    let login: String?
    let profileLink: String?
    let avatarURL: String?
    var avatar: ImageModel?
    
    init(with dict: [String:Any]) {
        self.id = dict["id"] as! Int
        self.avatarURL = dict["avatar_url"] as? String
        self.login = dict["login"] as? String
        self.profileLink = dict["html_url"] as? String
        
        if avatarURL != "" && avatarURL != nil {
            let image = ImageModel.init(withURL: (self.avatarURL)!)
            self.avatar = image
        }
    }
    
    init() {
        self.id = 0
        self.login = ""
        self.profileLink = ""
        self.avatarURL = ""
    }
    
    //MARK: --
    //MARK: Equatable protocol
    
    static func == (lhs: User, rhs: User) -> Bool {
        let equal = lhs.id == rhs.id
        return equal
    }
}
