import UIKit
import ReachabilitySwift

class Fetch: NSObject {
    
    //Making fetch request for lastUsers using link and userID for possibility to fetch 'next 25 users'
    
    class func fetchLast(with link: String, userId: Int, completion: @escaping (Array<Any>) -> Swift.Void) {
        let madeLink = String(format: "%@?since=%i&per_page=25", link, userId)
        let request: NSURLRequest = makeRequest(link: madeLink)
        fetch(request: request, completion: {json in
            completion(json)
        })
    }
    
    //Making fetch for subscribers
    
    class func fetchSubscribers(from: String, completion: @escaping (Array<Any>) -> Swift.Void) {
        let request: NSURLRequest = makeRequest(link: from)
        fetch(request: request, completion: {json in
            completion(json)
        })
    }
    
    //Next two methods can be separated to another class but we have only 2 requests so I've made them just private
    
    //MARK: --
    //MARK: Private Methods
    
    //Making universal URLRequest
    
    private class func makeRequest(link: String) -> NSURLRequest {
        let url: NSURL = NSURL(string: link)!
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "GET"
        
        return request
    }
    
    //Making universal fetch
    
    private class func fetch(request: NSURLRequest, completion: @escaping (Array<Any>) -> Swift.Void) {
        let session = URLSession.shared
        let reachability = Reachability()!
        
        if reachability.isReachable {
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard error == nil else {
                    return
                }
                guard let data = data else {
                    return
                }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? Array<Any>{
                        completion(json)
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            task.resume()
        } else {
            let notReachable = ["id": 0,
                                "login": "Internet is not reachable",
                                "html_url": "Sorry guys :(",
                                "avatar_url": ""] as [String : Any]
            let array: Array<Any> = Array.init(arrayLiteral: notReachable)
            
            completion(array)
        }
    }
}
